package AztecTestAutomation;

/**
* This test focuses on the Services link
* @author Rachel Marum
* @version 1.0
* @date 04/04/2016
**/

import java.util.regex.Pattern;
import java.util.concurrent.TimeUnit;
import org.junit.*;
import static org.junit.Assert.*;
import static org.hamcrest.CoreMatchers.*;
import org.openqa.selenium.*;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.support.ui.Select;

public class aztecServices {
  private WebDriver driver;
  private String baseUrl;
  private boolean acceptNextAlert = true;
  private StringBuffer verificationErrors = new StringBuffer();

  @Before
  public void setUp() throws Exception {
    //Initialise the web driver and browser
    driver = new FirefoxDriver();
    baseUrl = "https://www.aztecexchange.com/";
    driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
  }

  @Test
  public void testAztecServices() throws Exception {
    //Open the site under test, www.aztecexchange.com
    driver.get(baseUrl + "/");
	
	//Verify that the phone number is present on the home page
    assertEquals("+1 646 233 2782", driver.findElement(By.linkText("+1 646 233 2782")).getText());
	
	//Click onto Finance Companies link
    driver.findElement(By.linkText("Finance Companies")).click();
	
	//Verify that the text Europe is present on the page
    assertEquals("Europe", driver.findElement(By.cssSelector("h3:contains(\"Europe\")")).getText());
	
	//Click on the Corporations link
    driver.findElement(By.linkText("Corporations")).click();
	
	//Verify that the text 'The Aztec process' is present on this page
    assertEquals("The Aztec process", driver.findElement(By.cssSelector("h2.process-title")).getText());
	
	//Click on the E-Invoice Companies link
    driver.findElement(By.linkText("E-Invoice Companies")).click();
	
	//Verify that the Aztec logo is present at the end of the page
    assertEquals("", driver.findElement(By.xpath("//div/div/a/img")).getText());
    driver.findElement(By.cssSelector("img")).click();
  }

  @After
  public void tearDown() throws Exception {
    //The browser is closed after the test
    driver.quit();
    String verificationErrorString = verificationErrors.toString();
    if (!"".equals(verificationErrorString)) {
      fail(verificationErrorString);
    }
  }

  private boolean isElementPresent(By by) {
    try {
      driver.findElement(by);
      return true;
    } catch (NoSuchElementException e) {
      return false;
    }
  }

  private boolean isAlertPresent() {
    try {
      driver.switchTo().alert();
      return true;
    } catch (NoAlertPresentException e) {
      return false;
    }
  }

  private String closeAlertAndGetItsText() {
    try {
      Alert alert = driver.switchTo().alert();
      String alertText = alert.getText();
      if (acceptNextAlert) {
        alert.accept();
      } else {
        alert.dismiss();
      }
      return alertText;
    } finally {
      acceptNextAlert = true;
    }
  }
}
