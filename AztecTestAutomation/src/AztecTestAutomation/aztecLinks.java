
package AztecTestAutomation;

/**
* This test focuses on all the links on the site
* @author Rachel Marum
* @version 1.0
* @date 04/04/2016
**/

import java.util.regex.Pattern;
import java.util.concurrent.TimeUnit;
import org.junit.*;
import static org.junit.Assert.*;
import static org.hamcrest.CoreMatchers.*;
import org.openqa.selenium.*;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.support.ui.Select;

public class aztecLinks {
  private WebDriver driver;
  private String baseUrl;
  private boolean acceptNextAlert = true;
  private StringBuffer verificationErrors = new StringBuffer();

  @Before
  public void setUp() throws Exception {
    //Initialise the web driver and browser
    driver = new FirefoxDriver();
    baseUrl = "https://www.aztecexchange.com/";
    driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
  }

  @Test
  public void testAztecLinks() throws Exception {
   //Open the site www.aztecexchange.com
    driver.get(baseUrl + "/");
	
	//Verify that the image is present on the home page
    assertTrue(isElementPresent(By.cssSelector("img")));
	
	//Click on the About Us link
    driver.findElement(By.linkText("About Us")).click();
	
	//Click on the Investors link
    driver.findElement(By.linkText("Investors")).click();
	
	//Click on the Suppliers link
    driver.findElement(By.linkText("Suppliers")).click();
	
	//Click on the Media link
    driver.findElement(By.linkText("Media")).click();
	
	//Click on the Log In link
    driver.findElement(By.cssSelector("a.log-in")).click();
  }

  @After
  public void tearDown() throws Exception {
    //Close down the browser after test completion
    driver.quit();
    String verificationErrorString = verificationErrors.toString();
    if (!"".equals(verificationErrorString)) {
      fail(verificationErrorString);
    }
  }

  private boolean isElementPresent(By by) {
    try {
      driver.findElement(by);
      return true;
    } catch (NoSuchElementException e) {
      return false;
    }
  }

  private boolean isAlertPresent() {
    try {
      driver.switchTo().alert();
      return true;
    } catch (NoAlertPresentException e) {
      return false;
    }
  }

  private String closeAlertAndGetItsText() {
    try {
      Alert alert = driver.switchTo().alert();
      String alertText = alert.getText();
      if (acceptNextAlert) {
        alert.accept();
      } else {
        alert.dismiss();
      }
      return alertText;
    } finally {
      acceptNextAlert = true;
    }
  }
}
