# README #


/**
* To Set Up and Run the Tests
* @author Rachel Marum
* @version 1.0
* @date 04/04/2016
**/

# Tools used #
Firebug
Selenium IDE
Eclipse
Selenium WebDriver
JUnit

# Two tests created #
aztecLinks.java
aztecServices.java

Open AztecTestAutomation to run each test as a JUnit.

# Other Information #
Each test was created using Firebug and Selenium IDE.
Each test was then exported from Selenium IDE to Java/JUnit4/WebDriver.
Each test can then be run from Eclipse as a JUnit test.
